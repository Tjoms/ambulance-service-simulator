package uk.ac.aber.cs21120.solution;

import uk.ac.aber.cs21120.hospital.IJob;

/**
 * The Job class simulates a patient in need.
 */
public class Job implements IJob, Comparable<IJob> {

    private int id;
    private int priority;
    private int duration;
    private int countedTicks = 0;
    private int timeSubmitted = -1;

    /**
     * A constructor for the Job class
     *
     * @param id is the identity of the job
     * @param priority is the priority of the job
     * @param duration is how long the job takes to finish
     */
    public Job(int id, int priority, int duration){
    this.id = id;
    this.priority = priority;
    this.duration = duration;
    }

    /**
     * A method that returns the ID
     * @return ID of the job
     */
    @Override
    public int getID() {
        return this.id;
    }

    /**
     * A method that returns the priority
     * @return priority of the job
     */
    @Override
    public int getPriority() {
        return this.priority;
    }

    /**
     * A method that simulates time in the form of ticks
     *  One tick = one update
     */
    @Override
    public void tick() {
        countedTicks++;
    }

    /**
     * A method that checks whether the job is complete or not
     * @return true if counted ticks is equal or more to duration
     */
    @Override
    public boolean isDone() {
        if(countedTicks < duration){
            return false;
        }
        return true;
    }

    /**
     * A method that returns the number of ticks since it was submitted
     * @param now is the current simulator tick number
     * @return the number of ticks since the job was submitted
     */
    @Override
    public int getTimeSinceSubmit(int now){
        if(timeSubmitted == -1){
            throw new RuntimeException();
        }
        return now - this.timeSubmitted;
    }

    /**
     * A method that stores the time at which the job was added to the simulator
     * @param time	is the time at which the job was added
     */
    @Override
    public void setSubmitTime(int time) {
        this.timeSubmitted = time;

    }

    /**
     * A method that compares the priority of this job and another job.
     * @param otherJob is the job that this job is being compared to
     * @return a negative, 0 or a positive.
     */
    @Override
    public int compareTo(IJob otherJob) {

        return this.priority - otherJob.getPriority();
    }
}
