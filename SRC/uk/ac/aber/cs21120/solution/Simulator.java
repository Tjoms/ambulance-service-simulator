package uk.ac.aber.cs21120.solution;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import uk.ac.aber.cs21120.hospital.IJob;
import uk.ac.aber.cs21120.hospital.ISimulator;

import java.util.*;

/**
 * The Simulator class is used to simulate how the ambulances count and jobs are
 * interacting on each other and creating different outcomes. These outcomes are
 * then displayed to the user.
 */
public class Simulator implements ISimulator {
    private Multimap<Integer, Integer> priorityCompletionTimes;
    private PriorityQueue<IJob> waitingJobs;
    private Set<IJob> runningJobs;

    private int availableAmbulances;
    private int tickCount;

    /**
     * Constructor for Simulator
     *
     * @param numberOfAmbulances is the number of ambulances in the simulator
     */
    public Simulator(int numberOfAmbulances) {
        this.availableAmbulances = numberOfAmbulances;
        this.waitingJobs = new PriorityQueue<>();
        this.runningJobs = new HashSet<>(getSizeForHashSet(numberOfAmbulances));
        this.priorityCompletionTimes = ArrayListMultimap.create();
        this.tickCount = 0;
    }

    /**
     * A method that add jobs to a list of waiting jobs.
     *
     * @param newJob is the job added to waiting jobs
     */
    @Override
    public void add(IJob newJob) {
        newJob.setSubmitTime(tickCount);
        waitingJobs.add(newJob);
    }

    /**
     * The tick() method is used to update the Simulator. One tick is one unit of time.
     * When one tick has passed, the method checks if there are any running completed jobs that needs to be removed.
     * Once a job has been removed, the ambulance(s) are then available.
     * While there are ambulances available and waiting jobs, they will get assigned and put into running jobs.
     * Finally increment the number of counted ticks
     */
    @Override
    public void tick() {
        int sizeOfHashSet = getSizeForHashSet(runningJobs.size());
        Set<IJob> jobsToRemove = new HashSet<>(sizeOfHashSet);
        //Run through all running jobs
        for (IJob job : runningJobs) {
            //Delete job if its done and set +1 ambulance as available
            if (job.isDone()) {
                //Add jobs to a set of jobs that needs to be removed from runningJobs
                jobsToRemove.add(job);
                availableAmbulances++;
                //Store time taken for each priority since job was submitted to the simulator
                priorityCompletionTimes.put(job.getPriority(), job.getTimeSinceSubmit(tickCount));
            }
            job.tick();
        }
        //Remove all completed jobs from running Jobs
        runningJobs.removeAll(jobsToRemove);
        //Add jobs to running Jobs if there are ambulances left
        while (availableAmbulances != 0 && !waitingJobs.isEmpty()) {
            IJob firstQueuedJob = waitingJobs.poll();
            firstQueuedJob.tick();
            runningJobs.add(firstQueuedJob);

            availableAmbulances--;
        }
        tickCount++;
    }

    /**
     * A method that returns how many units of time have passed (ticks);
     *
     * @return how many ticks have passed.
     */
    @Override
    public int getTime() {
        return tickCount;
    }

    /**
     * A method that checks if there are any jobs in the simulator
     *
     * @return true if there are no jobs in the simulator
     */
    @Override
    public boolean allDone() {
        if (waitingJobs.isEmpty() && runningJobs.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * A method that returns a set of ID's from currently running jobs
     *
     * @return a set of ID's
     */
    @Override
    public Set<Integer> getRunningJobs() {
        int sizeOfHashSet = getSizeForHashSet(runningJobs.size());
        Set<Integer> ID = new HashSet<>((sizeOfHashSet));
        for (IJob job : runningJobs) {
            ID.add(job.getID());
        }
        return ID;
    }

    /**
     * A method that calculates the average completion time for a priority level.
     *
     * @param priority the priority level for which we wish find the average completion time.
     * @return the average completion time or 0 if there is no completed jobs for this priority.
     */
    @Override
    public double getAverageJobCompletionTime(int priority) {
        int totalCompletionTime = 0;
        int totalNumberOfTimes = 0;
        //Get every completion time and add them together
        //Also count the number of added completion times
        for (int completionTime : priorityCompletionTimes.get(priority)) {
            totalCompletionTime += completionTime;
            totalNumberOfTimes++;
        }
        //If there is a priority with a completion time
        // then calculate the average completion time and return it
        if (totalNumberOfTimes > 0) {
            double averageCompletionTime = (double) totalCompletionTime / (double) totalNumberOfTimes;
            return averageCompletionTime;
        }
        return 0;
    }

    /**
     * A method that returns available ambulances
     *
     * @return available ambulances
     */
    public int getAvailableAmbulances() {

        return availableAmbulances;
    }

    /**
     * A method that returns all waiting jobs priority in order
     *
     * @return the priorities of all waiting jobs.
     */
    public int[] getWaitingJobs() {

        IJob jobs[] = new IJob[waitingJobs.size()];
        int priorities[] = new int[waitingJobs.size()];
        int i = 0;

        //Have to use the 'poll()' method to get the items in order, it removes the first item in the PriorityQ
        //'peak()' does not work because i cant look past the first item in the PriorityQ
        while (!waitingJobs.isEmpty()) {
            IJob job = waitingJobs.poll();
            priorities[i] = job.getPriority();
            jobs[i] = job;

            i++;
        }

        //Adds all the jobs back into the PriorityQ
        for (IJob job : jobs) {
            waitingJobs.add(job);
        }
        return priorities;
    }

    /**
     * To avoid rehashing and increase efficiency we make the size of the HashSet:
     * sizeOfHashSet = numberOfElements/loadFactor
     * Since we usually get a double, we have to round up to the next int
     * to make sure the HashSet does not reach the loadFactor
     */
    private int getSizeForHashSet(int numberOfElements) {
        double sizeDouble = numberOfElements / 0.75;
        int sizeInt = (int) sizeDouble;
        double roundUp = sizeDouble - (double) sizeInt;
        if (roundUp > 0) {
            return sizeInt + 1;
        }
        return sizeInt;
    }
}
