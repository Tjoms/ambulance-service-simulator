package uk.ac.aber.cs21120.solution.Tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import uk.ac.aber.cs21120.solution.RandomGenerator;

public class RandomGeneratorTest {
    /**
     * A test that checks if the random value generated is between
     * the different intervals.
     */
    @Test
    public void testRandomValueFromIntervals(){

        for(int lowerBound = 0; lowerBound<100; lowerBound++){
            for(int upperBound =0+lowerBound; upperBound<200; upperBound++){
                testGettingRandomValueFromInterval(lowerBound, upperBound);

            }
        }
    }

    /**
     * A test that returns true if the random value generated is
     * between the wanted interval
     * @param lowerBound is the lower bound of the interval
     * @param upperBound is the upper bound of the interval
     */
   @Test
    private void testGettingRandomValueFromInterval(int lowerBound, int upperBound ) {
        RandomGenerator randomGenerator = new RandomGenerator();
        int runtime = 1000;
        boolean isBetweenInterval = false;
        int valueFromInterval = 0;

        for (int i = 0; i < runtime; i++) {
            valueFromInterval = randomGenerator.randomValueFromInterval(lowerBound, upperBound);
            if (valueFromInterval >= lowerBound && valueFromInterval <= upperBound) {
                isBetweenInterval = true;
            } else {
                isBetweenInterval = false;
            }

            Assertions.assertEquals(true, isBetweenInterval);
        }
    }

    /**
     * A test that checks if the set chanceOfSuccess outcome is the same as the chanceOfSuccess set by the user.
     * This test has a randomValueFromInterval of set chances from 0-1 (0%-100%) to not make the algorithm to complicated.
     * The chances are run 1000 times to make sure they are consistent
     *
     * The test is run 1 000*10*(10/2) =  50 000 times
     */
    @Test
    public void testChanceOfSuccess(){
        boolean passedTest = false;
        for(int runCount = 0; runCount < 1000; runCount++) {
            for (int numerator = 1; numerator < 10; numerator++) {

                for (int denominator = 1 + numerator; denominator < 10; denominator++) {
                    passedTest = testChanceBoundaries(numerator, denominator);
                    Assertions.assertEquals(true, passedTest);
                }
            }
        }
    }

    /**
     * A method that returns true if the chanceOfSuccess outcome is +-2.5% of the chanceOfSuccess set by the user
     * The chanceOfSuccess set is the numerator/denominator
     * @param numerator is the numerator
     * @param denominator is the denominator
     *
     * @return true if the chanceOfSuccess is +-2.5% of the set chanceOfSuccess
     */
    private boolean testChanceBoundaries(int numerator, int denominator ) {
        RandomGenerator randomGenerator = new RandomGenerator();
        double chance = (double) numerator / (double) denominator;
        int runtime = 10000;
        int countTrue = 0;
        int countFalse = 0;
        boolean isBetweenChance = false;
        for (int i = 0; i < runtime; i++) {
            if (randomGenerator.chanceOfSuccess(numerator, denominator)) {
                countTrue++;
            } else {
                countFalse++;
            }
        }
        System.out.println(countFalse + " and " + countTrue);

        double chanceOutcome = (double) countTrue / (double) runtime;
        double chanceMax = chanceOutcome + 0.025;
        double chanceMin = chanceOutcome - 0.025;
        if (chance < chanceMax && chance > chanceMin) {
            isBetweenChance = true;
        }

        return isBetweenChance;
    }
}