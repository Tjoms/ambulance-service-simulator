package uk.ac.aber.cs21120.solution;

import uk.ac.aber.cs21120.hospital.IJob;
import uk.ac.aber.cs21120.hospital.RandomPriorityGenerator;

/**
 * The Application class is used to run task3() and task4();
 */
public class Application {
    public static void main(String[] args) {
        Application app = new Application();
        app.task3();
        app.task4();
    }

    /**
     * This method is running the simulator with 4 ambulances
     */

    private void task3(){
        runSimulator(new Simulator(4));
    }

    /**
     * A method that does exactly the same as task3(), but runs multiple times using
     * different number of available ambulances to simulate how the difference in average
     * priority time changes.
     */
    private void task4(){
        for (int ambulances = 4; ambulances < 21; ambulances++) {
            runSimulator(new Simulator(ambulances));
        }
    }

    /**
     * A method that runs the simulator for a number of ticks.
     * While there are still jobs in running jobs, continue until they are finished.
     * During every tick a display of the current state of the simulator is displayed
     * Once the simulator is finished, an average time is then displayed
     *
     * @param simulator is the simulator used.
     */
    private static void runSimulator(Simulator simulator) {
        int totalAmbulances = simulator.getAvailableAmbulances();
        int ID = 0;
        //Total of ticks to run the simulator
        int totalTicks = 10000;
        int currentTicks = 0;

        //Running the simulator for a total number of ticks
        while (currentTicks < totalTicks || !simulator.getRunningJobs().isEmpty()) {

            //Create random number generators
            RandomPriorityGenerator priorityGenerator = new RandomPriorityGenerator();
            RandomGenerator randomGenerator = new RandomGenerator();
            //Add job if you have 1/3 chanceOfSuccess.
            //stop adding jobs after totalTicks is reached
            if (randomGenerator.chanceOfSuccess(1, 3) && currentTicks <= totalTicks) {
                //get duration between 10-20
                int duration = randomGenerator.randomValueFromInterval(10, 20);
                //get priority between 0-3
                int priority = priorityGenerator.next();
                //Create and add job to the simulator
                IJob job = new Job(ID, priority, duration);
                simulator.add(job);
                ID++;
            }
            //Update
            simulator.tick();
            currentTicks++;
        }
        //Print to console
        System.out.println("This simulator ran with a total of " + totalAmbulances + " ambulances");
        printAverageTimeForPriorities(simulator);
        System.out.println();
    }

    /**
     * A method that prints out the average time for all possible priorities.
     *
     * @param simulator is the simulator used to get the different priorities.
     */
    private static void printAverageTimeForPriorities(Simulator simulator) {
        //Print out the average time for all priorities
        int[] priorities = RandomPriorityGenerator.getPossiblePriorities();
        for (int priority : priorities) {
            double averageTime = simulator.getAverageJobCompletionTime(priority);
            System.out.println("For priority " + priority + " the average completion time was: " + averageTime);
        }
    }
}
