package uk.ac.aber.cs21120.solution.Tests;

import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import uk.ac.aber.cs21120.solution.Job;
import uk.ac.aber.cs21120.solution.Simulator;

/**
 * This class contains some basic JUnit tests for the Simulator class,
 * and those functions of Job which require the Simulator.
 * <p>
 * Please extend to add more tests as part of your testing strategy.
 *
 * @author Jim Finnis (jcf12@aber.ac.uk)
 */
public class SimulatorTest {

    /**
     * This tests that the tick() timekeeping works - i.e. that getTime() returns
     * the number of times the simulator has been ticked.
     */
    @Test
    public void testSimulatorTime() {
        Simulator s = new Simulator(4); // number of ambulances doesn't matter
        // run 10 ticks
        for (int i = 0; i < 10; i++)
            s.tick();
        // and check that the current time is 10 ticks.
        Assertions.assertEquals(10, s.getTime());
    }

    /**
     * Ensure that we can add a job, and that getTimeSinceSubmit() works.
     */
    @Test

    public void testJobAdd() {
        Simulator s = new Simulator(4);
        // run 10 ticks
        for (int i = 0; i < 10; i++)
            s.tick();

        // add a job
        Job j = new Job(1, 2, 3);
        s.add(j);

        // run 20 more ticks
        for (int i = 0; i < 20; i++)
            s.tick();

        // ensure that the job knows it's been submitted for 20 ticks
        Assertions.assertEquals(20, j.getTimeSinceSubmit(s.getTime()));
    }

    /**
     * Ensure that we can add multiple jobs and get the correct list of IDs
     * from getRunningJobs()
     */

    @Test
    public void testGetRunningJobs() {
        Simulator s = new Simulator(100); // huge number of ambulances so all jobs can run!
        // create 10 jobs with IDs 0-9. We don't care about priority and duration.
        for (int i = 0; i < 10; i++) {
            Job j = new Job(i, 1, 2);
            // add each job!
            s.add(j);
        }
        // run for 1 tick - this should move all jobs into the running set.
        s.tick();
        // get the set of running job IDs
        Set<Integer> set = s.getRunningJobs();
        // ensure that the set consists of 10 members
        Assertions.assertEquals(10, set.size());
        // and that all integers from 0 to 9 are in it.
        for (int i = 0; i < 10; i++) {
            Assertions.assertTrue(set.contains(i));
        }
    }

    /**
     * A method that test the removal of an available ambulance when jobs are added
     */
    @Test
    public void testRemoveAmbulances() {
        Simulator s = new Simulator(5);
        // Adds 3 jobs to the simulator
        for (int ID = 0; ID < 3; ID++) {
            Job j = new Job(ID, 1, 5); //Priority does not matter in this test
            s.add(j);
        }
        s.tick();
        Assertions.assertEquals(2, s.getAvailableAmbulances());
    }

    /**
     * A method that test adding back an ambulance to available
     * ambulances after a job has completed
     */
    @Test
    public void testAddingAmbulancesAfterCompletedJobs() {
        //Add 5 ambulances to the simulator
        Simulator s = new Simulator(5);
        // Adds 3 jobs to the simulator
        for (int ID = 0; ID < 3; ID++) {
            Job j = new Job(ID, 2, 5); //Priority does not matter in this test
            s.add(j);
        }

        //run 10 ticks - jobs will have been completed
        for (int tick = 0; tick < 10; tick++)
            s.tick();

        //Jobs have had enough time to complete their jobs so all ambulances should be available
        Assertions.assertEquals(5, s.getAvailableAmbulances());

    }

    /**
     * A method that tests getting the order of the priorityQueue where
     * every job has a different priority and are added in sorted order.
     */
    @Test
    public void testReadingWaitingJobQueueSorted() {
        // No ambulances makes sure the jobs are not removed from priority queue
        Simulator s = new Simulator(0);

        //Create 3 jobs with different priorities and add them to the simulator
        for(int i = 1; i<5; i++) {
            Job j = new Job(i, i, 5);
            s.add(j);
        }

        //Get waiting jobs from simulator
        int[] priorityJob = s.getWaitingJobs();
        String priorityString = "";
        //Create a string with job priorities
        for (int priority : priorityJob) {
            priorityString += priority;
        }

        Assertions.assertEquals("1234", priorityString);
    }
    /**
     * A method that tests getting the order of the priorityQueue where
     * there exists multiple jobs with the same priority and are added randomly.
     */
    @Test
    public void testReadingPriorityQueueUnsorted(){
        // No ambulances makes sure the jobs are not removed from priority queue
        Simulator s = new Simulator(0);

        //Create 3 jobs with different priorities and add them to the simulator
        Job j1 = new Job(1, 2, 5);
        Job j2 = new Job(2, 1, 5);
        Job j3 = new Job(3, 4, 5);
        Job j4 = new Job(4, 2, 5);
        s.add(j1);
        s.add(j2);
        s.add(j3);
        s.add(j4);

        //Get waiting jobs from simulator
        int[] priorityJob = s.getWaitingJobs();
        String priorityString = "";
        //Create a string with job priorities
        for (int priority : priorityJob) {
            priorityString += priority;
        }

        Assertions.assertEquals("1224", priorityString);
    }

    /**
     * A method that tests getting the average completion time for a priority
     * This test will get the jobs running straight away, which means no waiting time
     * in the priority queue.
     */
    @Test
    public void testGetAverageCompletionTime() {
        Simulator s = new Simulator(1000);

        //Add 4 jobs with the same priority to the simulator
        Job j1 = new Job(1, 2, 2);
        Job j2 = new Job(2, 2, 4);
        Job j3 = new Job(3, 2, 4);
        Job j4 = new Job(4, 2, 5);
        s.add(j1);
        s.add(j2);
        s.add(j3);
        s.add(j4);

        //After 10 ticks every job should have been completed
        for (int i = 0; i < 10; i++) {
            s.tick();
        }

        //Read the average completion time since the job was submitted to the simulator
        Assertions.assertEquals(3.75, s.getAverageJobCompletionTime(2));

    }
    /**
     * A method that tests getting the average completion time for a priority
     * This test will take inn account for the waiting time in the waiting queue.
     */
    @Test
    public void testGetAverageCompletionTimeWhenJobHasToWait() {
        Simulator s = new Simulator(3);
        Job j5 = new Job(1, 2, 5);
        s.add(j5);
        Job j6 = new Job(2, 3, 5);
        s.add(j6);
        Job j7 = new Job(3, 1, 5);
        s.add(j7);
        Job j8 = new Job(4, 3, 5);
        s.add(j8);

        for (int i = 0; i < 23; i++) {
            s.tick();
        }
        /* Average time will be 16 because only 3 ambulances available
         * so j7 have to wait for j4 to finish before it starts running.
         * This will take 22 ticks, 10(j4 wait time)+12(j7 duration) = 22.
         *
         * Then average time will be (10+22)/2 = 32/2 = 16 ticks
         */
        Assertions.assertEquals(7.5, s.getAverageJobCompletionTime(3));
    }
}
